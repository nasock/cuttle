package cuttle;

public enum CardValue {
	TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE;

	public int getNumValue() {
		return ordinal();
	}
	
	public static boolean isFace(CardValue v) {
		if(v.ordinal() > TEN.ordinal()) {
			return true;
		}
		return false;
	}
	
	public static boolean isFace(Card c) {
		return isFace(c.getValue());
	}
}
