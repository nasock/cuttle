package cuttle;

public class HoagiesCombination extends Combination {

	@Override
	public boolean isPresent(Deck d) {
		if(d.getSize() < 4) {
			return false;
		}
		Card firstCard = d.getFirstCard();
		Card fourthCard = d.getFourthCard();
		if (firstCard.getValue().equals(fourthCard.getValue())) {
			return true;
		}
		return false;
	}

}
