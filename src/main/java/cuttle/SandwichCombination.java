package cuttle;

public class SandwichCombination extends Combination{

	@Override
	public boolean isPresent(Deck d) {
		if(d.getSize() < 3) {
			return false;
		}
		
		Card firstCard = d.getFirstCard();
		Card thirdCard = d.getThirdCard();
		if(firstCard.getValue().equals(thirdCard.getValue())) {
			return true;
		}
		return false;
	}

}
