package cuttle;

public class PutCardEvent extends Event {

	public PutCardEvent(Player p) {
		super(p);
	}

	@Override
	public void accept(Cuttle game) {
		game.onEvent(this);
	}
}
