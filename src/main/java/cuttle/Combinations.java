package cuttle;

import java.util.ArrayList;

public class Combinations {
	private ArrayList<Combination> combinations;
	
	public Combinations() {
		combinations = new ArrayList<Combination> ();
		DoubleCombination d = new DoubleCombination();
		SandwichCombination s = new SandwichCombination();
		HoagiesCombination h = new HoagiesCombination();
		MarigeCombination m = new MarigeCombination();
		OrderCombination o = new OrderCombination();
		combinations.add(d);
		combinations.add(s);
		combinations.add(h);
		combinations.add(m);
		combinations.add(o);
	}
	
	public boolean isPresent(Deck d) {
		for (Combination c : combinations) {
			if(c.isPresent(d)) {
				return true;
			}
		}
		return false;
	}
}
