package cuttle;

public class EndTurnEvent extends Event {

	public EndTurnEvent(Player p) {
		super(p);
	}

	@Override
	public void accept(Cuttle game) {
		game.onEvent(this);
	}
}
