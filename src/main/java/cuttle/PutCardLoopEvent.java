package cuttle;

public class PutCardLoopEvent extends Event {

	public PutCardLoopEvent(Player p) {
		super(p);
	}

	@Override
	public void accept(Cuttle game) {
		game.onEvent(this);
	}

}
