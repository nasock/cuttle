package cuttle;

import java.util.ArrayList;

public class Logger implements GameListener {
	interface LoggerListener {
		public void onLog(String message);
		public void onClear();
	}

	private ArrayList<String> log;
	private ArrayList<LoggerListener> listeners;

	public Logger() {
		log = new ArrayList<String>();
		listeners = new ArrayList<LoggerListener>();
	}

	private void addMessage(String message) {
		log.add(message);
		for (LoggerListener listener : listeners) {
			listener.onLog(message);
		}
	}

	public void addListener(LoggerListener listener) {
		listeners.add(listener);
	}

	public void removeListener(LoggerListener listener) {
		listeners.remove(listener);
	}

	public void clear() {
		log.clear();
		for (LoggerListener listener : listeners) {
			listener.onClear();
		}
	}
	
	public void onPutCard(Cuttle g, Player p, Card c) {
		String string = (p + " put card " + c);
		addMessage(string);
	}

	public void onPlayerDeckGrab(Cuttle g, Player p) {
		String string = (p + " grab the deck");
		addMessage(string);
	}

	
	public void onPlayerHasNoCards(Cuttle g, Player p) {
		String string = (p + " has no cards");
		addMessage(string);
	}

	public void onChangePlayer(Cuttle g, Player p) {
		String string = (p + " turn");
		addMessage(string);
	}

	public void onComplete(Cuttle g, Player p) {
		String string = ("game is over. " + p + " won");
		addMessage(string);
	}

	public void onSlap(Cuttle g, Player p, boolean success) {
		String s;
		if (success) {
			s = "successfully";
		} else {
			s = "unsuccessfully";
		}
		String string = (p + " slapped " + s);
		addMessage(string);
	}

	public String toString() {
		StringBuilder strb = new StringBuilder();
		for (String string : log) {
			strb.append(string + "\n");
		}
		return strb.toString();
	}

}
