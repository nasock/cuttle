package cuttle;

public class SlapEvent extends Event {

	public SlapEvent(Player p, long l) {
		super(p, l);
	}

	@Override
	public void accept(Cuttle game) {
		game.onEvent(this);
	}
}
