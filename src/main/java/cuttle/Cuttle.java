package cuttle;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;

public class Cuttle {
	private Deck gameDeck;
	private ArrayList<Player> players;
	private int curPlayerIndex;
	private HashMap<CardValue, Integer> faceToTries;
	private ArrayList<GameListener> listeners;
	private Combinations combinations;
	private Player currentPlayer;
	private Player previousPlayer;
	private boolean complete;
	private LinkedList<Event> events;

	public Cuttle() {
		this.gameDeck = new Deck();
		this.faceToTries = new HashMap<CardValue, Integer>();
		faceToTries.put(CardValue.JACK, 1);
		faceToTries.put(CardValue.QUEEN, 2);
		faceToTries.put(CardValue.KING, 3);
		faceToTries.put(CardValue.ACE, 4);
		this.listeners = new ArrayList<GameListener>();
		this.combinations = new Combinations();
		this.events = new LinkedList<Event>();
	}

	private Deck makeDeck() {
		Deck d = new Deck();
		for (CardSuit s : CardSuit.values()) {
			for (CardValue v : CardValue.values()) {
				d.add(new Card(v, s));
			}
		}
		long rgenseed = System.currentTimeMillis();
//		d.shuffle(1542813825046L);
		d.shuffle(rgenseed);
//		System.out.println("Random number generator seed is " + rgenseed);
		return d;
	}

	public int getGameDeckSize() {
		return gameDeck.getSize();
	}

	public int getPlayersSize() {
		return players.size();
	}

	public Player getPlayer(int i) {
		return players.get(i);
	}

	public boolean checkCombinations(Combinations combinations) {
		return combinations.isPresent(gameDeck);
	}

	public Player getPreviousPlayer() {
		return previousPlayer;
	}

	public void addListener(GameListener l) {
		listeners.add(l);
	}

	public void removeListener(GameListener l) {
		listeners.remove(l);
	}

	private void notifyOnPutCard(Player p, Card c) {
		for (GameListener l : listeners) {
			l.onPutCard(this, p, c);
		}
	}

	private void notifyOnPlayerDeckGrab(Player p) {
		for (GameListener l : listeners) {
			l.onPlayerDeckGrab(this, p);
		}
	}

	private void notifyOnPlayerHasNoCards(Player p) {
		for (GameListener l : listeners) {
			l.onPlayerHasNoCards(this, p);
		}
	}

	private void notifyOnChangePlayer(Player p) {
		for (GameListener l : listeners) {
			l.onChangePlayer(this, p);
		}
	}

	private void notifyOnComplete(Player p) {
		for (GameListener l : listeners) {
			l.onComplete(this, p);
		}
	}

	private void notifyOnSlap(Player p, boolean success) {
		for (GameListener l : listeners) {
			l.onSlap(this, p, success);
		}
	}

	public void addEvent(Event event) {
		events.add(event);
	}

	private Player getCurrentPlayer() {
		return players.get(curPlayerIndex);
	}

	private Player calcPreviousPlayer() {
		int i = curPlayerIndex;
		if (i == 0) {
			i = players.size();
		}
		return players.get(i - 1);
	}

	private Player getNextPlayer() {
		curPlayerIndex++;
		curPlayerIndex = curPlayerIndex % players.size();
		Player p = getCurrentPlayer();
		if (p.isEmpty()) {
			p = getNextPlayer();
		}
		return p;
	}

	private boolean isOnlyOneNotEmpty() {
		int countNotEmpty = 0;
		for (Player p : players) {
			if (!p.isEmpty()) {
				countNotEmpty++;
			}
		}
		if (countNotEmpty == 1) {
			return true;
		}
		return false;
	}

	public Card getTopCard() {
		if (gameDeck.getSize() == 0) {
			return null;
		}
		return gameDeck.getCard(gameDeck.getSize() - 1);
	}

	private Card putCard(Player p) {
		Card c = p.popCard();
		gameDeck.add(c);
		notifyOnPutCard(p, c);
		return c;
	}

	public int countTries(Card c) {
		return faceToTries.get(c.getValue());
	}

	private void clearSlaps() {
		LinkedList<Event> newEvents = new LinkedList<Event>();
		for (Event event : events) {
			if (!(event instanceof SlapEvent)) {
				newEvents.add(event);
			}
		}
		events = newEvents;
	}

	private void slap(Player p) {
		boolean success;
		if (combinations.isPresent(gameDeck)) {
			p.addDeck(gameDeck);
			gameDeck.clear();
			notifyOnPlayerDeckGrab(p);
			success = true;
			clearSlaps();
		} else {
			int size = p.getDeckSize();
			Deck newd;
			if (size == 1) {
				newd = p.popDeck(1);
				gameDeck.addDeckToTop(newd);
			} else if (size >= 2) {
				newd = p.popDeck(2);
				gameDeck.addDeckToTop(newd);
			}
			success = false;
		}
		notifyOnSlap(p, success);
	}

	private boolean isComplete() {
		return complete;
	}

	private void grabDeck(Player player) {
		player.addDeck(gameDeck);
		gameDeck.clear();
		notifyOnPlayerDeckGrab(player);
	}

	private boolean checkComplete() {
		if (previousPlayer.isEmpty() && isOnlyOneNotEmpty()) {
			Player winer = players.get(0);
			for (Player p : players) {
				if (!p.isEmpty()) {
					winer = p;
				}
			}
			notifyOnComplete(winer);
			complete = true;
		}
		return complete;
	}

	private void endTurn() {
		if (checkComplete()) {
			return;
		}
		previousPlayer = currentPlayer;
		currentPlayer = getNextPlayer();
		notifyOnChangePlayer(currentPlayer);
	}

	private void putCardLoop(Player player) {
		putCard(player);
		Card currCard = getTopCard();
		if (currCard.isFace()) {
			player.endLoop(this);
		}
	}

	public void processEvents() {
		events.sort(new Comparator<Event>() {
			public int compare(Event o1, Event o2) {
				long t1 = o1.executionTime;
				long t2 = o2.executionTime;
				if(t1 == t2) {
					return 0;
				} else if(t1 < t2) {
					return -1;
				} else {
					return 1;
				}
			}
		});

		Event event = events.getFirst();
		long currTime = System.currentTimeMillis();
		if (event.executionTime < currTime) {
			events.removeFirst();
			event.accept(this);
		}
	}

	public void onEvent(PutCardEvent e) {
		putCard(e.player);
	}

	public void onEvent(PutCardLoopEvent e) {
		putCardLoop(e.player);
	}

	public void onEvent(GrabDeckEvent e) {
		grabDeck(e.player);
	}

	public void onEvent(SlapEvent e) {
		slap(e.player);
	}

	public void onEvent(EndTurnEvent e) {
		endTurn();
	}

	public void update(int delta) {
		if (isComplete()) {
			return;
		}

		if (events.isEmpty()) {
			currentPlayer.makeTurn(this);
		}
		// for faster event processing
		if (!events.isEmpty()) {
			processEvents();
		}
	}
	
	/**
	 * Notifies players for changes in settings
	 * @param sett game settings
	 */
	public void onChangeSettings(Settings sett) {
		for(Player player : players) {
			player.onChangeSettings(sett);
		}
	}

	public void start(Settings settings) {
		Deck d = makeDeck();
		int count = settings.getActivePlayerCount();
		ArrayList<Deck> splitted = d.split(count);
		players = new ArrayList<Player>(count);
		for (int i = 0; i < count; i++) {
			Deck deck = splitted.get(i);
			PlayerInfo plInf = settings.getPlayerInfo(i);
			String name = plInf.getName();
			boolean human = plInf.getIsHuman();
			if (human) {
				HumanPlayer player = new HumanPlayer(i, name, deck);
				player.init(this);
				players.add(player);
			} else {
				double chance = settings.getChance();
				long slapTime = settings.getSlapTime();
				AIPlayer player = new AIPlayer(i, name, deck, chance, slapTime);
				player.init(this);
				addListener(player);
				players.add(player);
			}
		}

		curPlayerIndex = 0;
		currentPlayer = getCurrentPlayer();
		previousPlayer = calcPreviousPlayer();
		complete = false;
	}

}
