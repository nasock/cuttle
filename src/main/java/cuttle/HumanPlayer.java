package cuttle;

public class HumanPlayer extends Player {

	public HumanPlayer(int _id, String n, Deck d) {
		super(_id, n, d, true);
		disable();
	}

	public Card popCard() {
		disable();
		return super.popCard();
	}
}
