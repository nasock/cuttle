package cuttle;

public class GrabDeckEvent extends Event {

	public GrabDeckEvent(Player p) {
		super(p);
	}

	@Override
	public void accept(Cuttle game) {
		game.onEvent(this);
	}

}
