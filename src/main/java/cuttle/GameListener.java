package cuttle;

public interface GameListener {
	public void onPutCard(Cuttle g, Player p, Card c);
	public void onPlayerDeckGrab(Cuttle g, Player p);
	public void onPlayerHasNoCards(Cuttle g, Player p);
	public void onChangePlayer(Cuttle g, Player p);
	public void onComplete(Cuttle g, Player p);
	public void onSlap(Cuttle g, Player p,boolean success);
}
