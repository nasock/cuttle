package cuttle;

public class PlayerInfo {
	
	private String name;
	private boolean isHuman;
	
	public PlayerInfo(String n, boolean h) {
		name = n;
		isHuman = h;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean getIsHuman(){
		return isHuman;
	}
}
