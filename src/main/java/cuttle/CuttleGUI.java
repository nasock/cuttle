package cuttle;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.WindowConstants;

public class CuttleGUI extends JFrame implements ActionListener, GameListener {
	private Settings settings;
	final private int WIDTH = 800;
	final private int HEIGHT = 720;
	final private String PATH = "data/cards/";
	private ImageLib imageLib;
	private Timer timer;
	private Logger log;
	private LogWindow logWindow;

	private ArrayList<String> menuItems;
	private final String MENU_TITLE = "Game";
	private final String MENU_NEW = "New Game";
	private final String PLAYERS_MENU_TWO = "For 2 players";
	private final String PLAYERS_MENU_THREE = "For 3 players";
	private final String PLAYERS_MENU_FOUR = "For 4 players";
	private final String DIFFICULTY_MENU_EASY = "Easy";
	private final String DIFFICULTY_MENU_MEDIUM = "Medium";
	private final String DIFFICULTY_MENU_HARD = "Hard";
	private final String MENU_EXIT = "Exit";

	private ArrayList<String> speedItems;
	private final String SPEED_MENU_TITLE = "Speed";
	private final String SPEED_MENU_SLOW = "Slow";
	private final String SPEED_MENU_NORMAL = "Normal";
	private final String SPEED_MENU_FAST = "Fast";

	private Zone[] zones;
	private HashMap<Integer, Zone> playerToZone;
	private Cuttle game;
	private boolean paused;

	final private String SLAP_STR = "slap";
	final private String NO_CARDS_STR = "no cards";
	final private int GRID_SIDE = 3;
	final private int BOTTOM_ZONE = 7;
	final private int TOP_ZONE = 1;
	final private int LEFT_ZONE = 5;
	final private int RIGHT_ZONE = 3;
	final private int CENTER_ZONE = 4;
	final private double SCALE = 0.6;
	final private Color TABLE_COLOR = new Color(24, 129, 54);
	final private Color TOOLBAR_COLOR = new Color(24, 129, 54);
	final private Color PLAYER_COLOR = new Color(248, 222, 126);
	final private Color ACTIVE_PLAYER_COLOR = new Color(255, 180, 80);
	final private Color EMPTY_ZONE_COLOR = new Color(84, 189, 114);
	final private Color WINNER_COLOR = new Color(224, 159, 114);
	final private static String[] PLAYER_NAMES = new String[] { "You", "Gapochka", "Stilniy Chuvachok", "Cartoshka" };

	/**
	 * Constructs main gui application. This is entry point for Ratscrew game. It's
	 * a main view responsible for user interface. It loads all card image data and
	 * setups all game components.
	 * 
	 * @param sett
	 *            game settings
	 * @throws IOException
	 *             if fails to load card images
	 */
	public CuttleGUI(Settings sett) throws IOException {
		super("CUTTLE");
		settings = sett;
		setBounds(50, 10, WIDTH, HEIGHT);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		imageLib = new ImageLib(PATH);
		timer = new Timer(settings.getSpeed(), this);
		log = new Logger();
		logWindow = new LogWindow(CuttleGUI.this, log);

		menuItems = new ArrayList<String>();
		menuItems.add(MENU_NEW);
		menuItems.add(PLAYERS_MENU_TWO);
		menuItems.add(PLAYERS_MENU_THREE);
		menuItems.add(PLAYERS_MENU_FOUR);
		menuItems.add(DIFFICULTY_MENU_EASY);
		menuItems.add(DIFFICULTY_MENU_MEDIUM);
		menuItems.add(DIFFICULTY_MENU_HARD);
		menuItems.add(MENU_EXIT);
		JMenu fileMenu = makeMenu(menuItems, MENU_TITLE);

		speedItems = new ArrayList<String>();
		speedItems.add(SPEED_MENU_SLOW);
		speedItems.add(SPEED_MENU_NORMAL);
		speedItems.add(SPEED_MENU_FAST);
		JMenu speedMenu = makeMenu(speedItems, SPEED_MENU_TITLE);

		JMenuBar menu = new JMenuBar();
		menu.add(fileMenu);
		menu.add(speedMenu);
		setJMenuBar(menu);

		addAllPanels();
	}

	/**
	 * Creates part of game menu and setups appropriate callbacks
	 * 
	 * @param itemNames
	 *            list of menu items
	 * @param menuName
	 *            name of menu
	 * @return JMenu instance
	 */
	private JMenu makeMenu(ArrayList<String> itemNames, String menuName) {
		JMenu fileMenu = new JMenu(menuName);
		for (String menuItem : itemNames) {
			JMenuItem item = new JMenuItem(menuItem);
			item.setActionCommand(menuItem);
			item.addActionListener(new MenuListener());
			fileMenu.add(item);
		}
		return fileMenu;
	}

	private class MenuListener implements ActionListener {
//		menu action handler
		public void actionPerformed(ActionEvent e) {
			String command = e.getActionCommand();
			if (MENU_NEW.equals(command)) {
				restart();
			} else if (PLAYERS_MENU_TWO.equals(command)) {
				restart(2);
			} else if (PLAYERS_MENU_THREE.equals(command)) {
				restart(3);
			} else if (PLAYERS_MENU_FOUR.equals(command)) {
				restart(4);
			} else if (DIFFICULTY_MENU_EASY.equals(command)) {
				settings.setEasyDifficulty();
				restart();
			} else if (DIFFICULTY_MENU_MEDIUM.equals(command)) {
				settings.setMediumDifficulty();
				restart();
			} else if (DIFFICULTY_MENU_HARD.equals(command)) {
				settings.setHardDifficulty();
				restart();
			} else if (MENU_EXIT.equals(command)) {
				System.exit(0);
			} else if (SPEED_MENU_SLOW.equals(command)) {
				settings.setSlowSpeed();
				updateTimerDelay();
			} else if (SPEED_MENU_NORMAL.equals(command)) {
				settings.setNormalSpeed();
				updateTimerDelay();
			} else if (SPEED_MENU_FAST.equals(command)) {
				settings.setFastSpeed();
				updateTimerDelay();
			}
		}
	}

	/**
	 * creates panels, game zones, toolbar
	 */
	private void addAllPanels() {
		Container container = getContentPane();

		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		buttonPanel.setBackground(TOOLBAR_COLOR);

		JPanel innerPanel = new JPanel();
		innerPanel.setLayout(new GridLayout(GRID_SIDE, GRID_SIDE));
		innerPanel.setBackground(EMPTY_ZONE_COLOR);

		createToolbar(buttonPanel);
		createZones(innerPanel);

		panel.add(BorderLayout.NORTH, buttonPanel);
		panel.add(BorderLayout.CENTER, innerPanel);
		container.add(panel);
		pack();
	}
	

	/**
	 * Setups toolbar button with image, color, borders and other params.
	 * @param button
	 * @param img button icon
	 */
	private void setupToolbarButton(JButton button, Image img) {
		button.setMargin(new Insets(0, 0, 0, 0));
		button.setBackground(TOOLBAR_COLOR);
		button.setBorder(null);
		button.setIcon(new ImageIcon(img));
		button.setEnabled(true);
	}
	

	/**
	 * Creates game toolbar with all specific logic and callbacks
	 * @param panel toolbar panel
	 */
	private void createToolbar(JPanel panel) {
		JButton restartButton = new JButton();
		Image restartIcon = imageLib.getRestartIcon();
		setupToolbarButton(restartButton, restartIcon);

		final JButton pauseButton = new JButton();
		Image pauseIcon = imageLib.getPauseIcon();
		setupToolbarButton(pauseButton, pauseIcon);

		JButton logtButton = new JButton();
		Image logIcon = imageLib.getLogIcon();
		setupToolbarButton(logtButton, logIcon);

		restartButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				restart();
			}
		});

		pauseButton.addMouseListener(new MouseAdapter() {
			Image pauseIcon = imageLib.getPauseIcon();
			Image playIcon = imageLib.getPlayIcon();

			public void mouseClicked(MouseEvent e) {
				if (paused) {
					paused = false;
					pauseButton.setIcon(new ImageIcon(pauseIcon));
					timer.start();
				} else {
					paused = true;
					pauseButton.setIcon(new ImageIcon(playIcon));
					timer.stop();
				}
			}
		});

		logtButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				toggleLog();
			}
		});

		panel.add(restartButton);
		panel.add(Box.createRigidArea(new Dimension(5, 0)));
		panel.add(pauseButton);
		panel.add(Box.createRigidArea(new Dimension(5, 0)));
		panel.add(logtButton);
		panel.add(Box.createRigidArea(new Dimension(5, 0)));
	}
	

	/**
	 * Changes game settings with new player count
	 * @param count new player count
	 */
	private void setPlayerCount(int count) {
		settings.setActivePlayerCount(count);
	}
	

	/**
	 * Toggles log window
	 */
	private void toggleLog() {
		logWindow.toggleLog();
	}
	

	/**
	 * Restarts game with current player count
	 */
	private void restart() {
		int count = settings.getActivePlayerCount();
		restart(count);
	}
	

	/**
	 * Restarts game. Recreate all gui elements
	 * @param countPlayers new player count
	 */
	private void restart(int countPlayers) {
		timer.stop();
		Container container = getContentPane();
		container.removeAll();
		setPlayerCount(countPlayers);
		addAllPanels();
		start();
	}
	
	

	/**
	 * Updates timer for new game step
	 */
	private void updateTimerDelay() {
		game.onChangeSettings(settings);
		timer.setDelay(settings.getSpeed());
	}

	/**
	 * Creates game zones
	 * @param panel parent panel
	 */
	private void createZones(JPanel panel) {
		zones = new Zone[] { new Zone(EMPTY_ZONE_COLOR), new Zone(PLAYER_COLOR), new Zone(EMPTY_ZONE_COLOR),
				new Zone(PLAYER_COLOR), new Zone(TABLE_COLOR), new Zone(PLAYER_COLOR), new Zone(EMPTY_ZONE_COLOR),
				new Zone(PLAYER_COLOR), new Zone(EMPTY_ZONE_COLOR) };

		for (Zone z : zones) {
			panel.add(z);
			z.setLayout(new BoxLayout(z, BoxLayout.PAGE_AXIS));
			z.setBorder(BorderFactory.createEmptyBorder(15, 0, 0, 0));
		}

		playerToZone = new HashMap<Integer, Zone>();
		int count = settings.getActivePlayerCount();
		putPlayerAndZoneToMap(count);
	}

	/**
	 * Links player id with zone. It's necessary to place players in correct zones
	 * @param count player count
	 */
	private void putPlayerAndZoneToMap(int count) {
		if (count == 2) {
			playerToZone.put(0, getZone(BOTTOM_ZONE));
			playerToZone.put(1, getZone(TOP_ZONE));
		} else if (count == 3) {
			playerToZone.put(0, getZone(BOTTOM_ZONE));
			playerToZone.put(1, getZone(RIGHT_ZONE));
			playerToZone.put(2, getZone(TOP_ZONE));
		} else if (count == 4) {
			playerToZone.put(0, getZone(BOTTOM_ZONE));
			playerToZone.put(1, getZone(RIGHT_ZONE));
			playerToZone.put(2, getZone(TOP_ZONE));
			playerToZone.put(3, getZone(LEFT_ZONE));
		}
	}

	/**
	 * Returns zone at the specified index.
	 * @param ind zone index
	 * @return zone
	 */
	private Zone getZone(int ind) {
		return zones[ind];
	}

	/**
	 * Adds hand to specific zone
	 * @param zone empty zone
	 * @param img cars image
	 * @param text hand label text
	 * @return new hand
	 */
	private Hand addHandToZone(Zone zone, Image img, String text) {
		Hand hand = new Hand(img, SCALE, text);
		zone.setHand(hand);
		return hand;
	}

	/**
	 * Adds mouse listener to hand that enables player on click.
	 * @param hand 
	 * @param player human player
	 */
	private void addMouseListenerToHand(Hand hand, final HumanPlayer player) {
		hand.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (paused == true) {
					return;
				}
				player.enable();
			}
		});
	}

	/**
	 * Adds slap button for human player.
	 * @param hand
	 * @param player human player
	 */
	private void addSlapButton(Hand hand, final HumanPlayer player) {
		JButton button = hand.addButton(SLAP_STR);
		button.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (paused == true) {
					return;
				}
				game.addEvent(new SlapEvent(player, 0));
			}
		});
	}

	/**
	 * Create new game, players, hands and player controls. Starts timer.
	 */
	private void start() {
		game = new Cuttle();
		game.addListener(this);
		game.addListener(log);
		game.start(settings);
		log.clear();
		paused = false;

		for (int i = 0; i < settings.getActivePlayerCount(); i++) {
			Player player = game.getPlayer(i);
			Zone zone = playerToZone.get(i);
			Image im = imageLib.getBack();
			String str = player.toString();

			Hand hand = addHandToZone(zone, im, str);

			if (i == 0) {
				zone.setBackground(ACTIVE_PLAYER_COLOR);
			} else {
				zone.setBackground(PLAYER_COLOR);
			}

			if (player.isHuman()) {
				HumanPlayer humanPlayer = (HumanPlayer) player;
				addMouseListenerToHand(hand, humanPlayer);
				addSlapButton(hand, humanPlayer);
			}
		}

		Image im = imageLib.getNoCards();
		addHandToZone(getZone(CENTER_ZONE), im, NO_CARDS_STR);

		validate();
		timer.start();
	}

	/**
	 * Returns zone for specific player.
	 * @param player
	 * @return player zone
	 */
	private Zone getPlayerZone(Player player) {
		int id = player.getId();
		return playerToZone.get(id);
	}

	/**
	 * @return centre hand
	 */
	private Hand getCenterHand() {
		Zone centerZone = getZone(CENTER_ZONE);
		Hand centerHand = centerZone.getHand();
		return centerHand;
	}

	/**
	 * Returns hand for specific player.
	 * @param player
	 * @return player hand
	 */
	private Hand getPlayerHand(Player player) {
		int id = player.getId();
		Zone zone = playerToZone.get(id);
		return zone.getHand();
	}

	/**
	 * Updates hand image and text from player.
	 * @param player
	 */
	private void updateHandImageAndTextFromPlayer(Player player) {
		Hand playerHand = getPlayerHand(player);
		Image im;
		if (player.isEmpty()) {
			im = imageLib.getNoCards();
		} else {
			im = imageLib.getBack();
		}
		playerHand.setImage(im);
		String str = player.toString();
		playerHand.setText(str);
	}

	/**
	 * Sets text to center zone.
	 * @param str centre zone text
	 */
	private void setCenterText(String str) {
		Hand centerHand = getCenterHand();
		centerHand.setText(str);
	}

	/**
	 * Sets card image to centre zone.
	 * @param card
	 */
	private void setCenterCard(Card card) {
		Hand centerHand = getCenterHand();
		Image im;
		if (card == null) {
			im = imageLib.getNoCards();
		} else {
			im = imageLib.getImage(card);
		}
		centerHand.setImage(im);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 * Timer callback. Updates game with game step.
	 */
	public void actionPerformed(ActionEvent e) {
		game.update(settings.getSpeed());
	}

	/* (non-Javadoc)
	 * @see ratscrew.GameListener#onPutCard(ratscrew.Ratscrew, ratscrew.Player, ratscrew.Card)
	 */
	public void onPutCard(Cuttle game, Player player, Card card) {
		updateHandImageAndTextFromPlayer(player);
		setCenterCard(card);
		int deckSize = game.getGameDeckSize();
		setCenterText("" + deckSize);
	}

	/* (non-Javadoc)
	 * @see ratscrew.GameListener#onPlayerDeckGrab(ratscrew.Ratscrew, ratscrew.Player)
	 */
	public void onPlayerDeckGrab(Cuttle game, Player player) {
		updateHandImageAndTextFromPlayer(player);
		setCenterCard(null);
		setCenterText(NO_CARDS_STR);
	}

	/* (non-Javadoc)
	 * @see ratscrew.GameListener#onPlayerHasNoCards(ratscrew.Ratscrew, ratscrew.Player)
	 */
	public void onPlayerHasNoCards(Cuttle game, Player player) {
	}

	/* (non-Javadoc)
	 * @see ratscrew.GameListener#onChangePlayer(ratscrew.Ratscrew, ratscrew.Player)
	 */
	public void onChangePlayer(Cuttle game, Player player) {
		for (int i = 0; i < playerToZone.size(); i++) {
			Zone zone = playerToZone.get(i);
			zone.setBackground(PLAYER_COLOR);
		}

		Zone playerZone = getPlayerZone(player);
		playerZone.setBackground(ACTIVE_PLAYER_COLOR);
	}

	/* (non-Javadoc)
	 * @see ratscrew.GameListener#onComplete(ratscrew.Ratscrew, ratscrew.Player)
	 */
	public void onComplete(Cuttle game, Player player) {
		timer.stop();
		for (int i = 0; i < game.getPlayersSize(); i++) {
			Player p = game.getPlayer(i);
			updateHandImageAndTextFromPlayer(p);
		}

		Zone playerZone = getPlayerZone(player);
		playerZone.setBackground(WINNER_COLOR);

		setCenterText(player.toString() + " IS WINNER");
	}

	/* (non-Javadoc)
	 * @see ratscrew.GameListener#onSlap(ratscrew.Ratscrew, ratscrew.Player, boolean)
	 */
	public void onSlap(Cuttle game, Player player, boolean success) {
		updateHandImageAndTextFromPlayer(player);
		if (success) {
			setCenterText(player.toString() + " SLAPPED");
		} else {
			setCenterText(player.toString() + " PUT 2 CARDS");
			Card card = game.getTopCard();
			setCenterCard(card);
		}
	}

	/**
	 * Creates settings using player names.
	 * @return new default settings
	 */
	private static Settings createDefaultSettings() {
		Settings set = new Settings();
		for (int i = 0; i < PLAYER_NAMES.length; i++) {
			String name = PLAYER_NAMES[i];
			if (i == 0) {
				set.addHumanPlayer(name);
			} else {
				set.addAIPlayer(name);
			}
		}
		return set;
	}

	public static void main(String[] args) throws IOException {
		Settings st = createDefaultSettings();
		CuttleGUI rs = new CuttleGUI(st);
		rs.start();
		rs.setVisible(true);
	}
}
