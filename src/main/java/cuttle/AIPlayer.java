package cuttle;

import java.util.Random;

public class AIPlayer extends Player implements GameListener {
	private double chance;
	private long slapTime;
	private Combinations combinations;

	public AIPlayer(int _id, String n, Deck d, double ch, long slT) {
		super(_id, n, d, false);
		chance = ch;
		slapTime = slT;
		combinations = new Combinations();
	}

	@Override
	public void onChangeSettings(Settings set) {
		slapTime = set.getSlapTime();
	}

	public void onPutCard(Cuttle game, Player p, Card c) {
		Random r = new Random();
		float f = r.nextFloat();
		if ((f < chance) && game.checkCombinations(combinations)) {
//			random deviation for slap time
			int rand = r.nextInt(200);
			game.addEvent(new SlapEvent(this, slapTime + rand));
		}
	}

	public void onPlayerDeckGrab(Cuttle g, Player p) {
	}

	public void onPlayerHasNoCards(Cuttle g, Player p) {
	}

	public void onChangePlayer(Cuttle g, Player p) {
	}

	public void onComplete(Cuttle g, Player p) {
	}

	public void onSlap(Cuttle g, Player p, boolean success) {
		if (success) {
			isSlapped = false;
		}
	}
}
