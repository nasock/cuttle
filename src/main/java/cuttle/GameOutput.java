package cuttle;

public class GameOutput implements GameListener {

	public void onPutCard(Cuttle g, Player p, Card c) {
		System.out.println(p + " put card " + c);
	}

	public void onPlayerDeckGrab(Cuttle g, Player p) {
		System.out.println(p + " grab the deck");
	}

	public void onPlayerHasNoCards(Cuttle g, Player p) {
		// System.out.println(p + " has no cards");
	}

	public void onChangePlayer(Cuttle g, Player p) {
		// System.out.println(p + " turn");
	}

	public void onComplete(Cuttle g, Player p) {
		System.out.println("game is over. " + p + " won");
	}

	public void onSlap(Cuttle g, Player p, boolean success) {
		String s;
		if (success) {
			s = "successfully";
		} else {
			s = "unsuccessfully";
		}
		System.out.println(p + " slapped " + s);
	}

}
