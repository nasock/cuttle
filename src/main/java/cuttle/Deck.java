package cuttle;

import java.util.ArrayList;
import java.util.Random;

public class Deck {
	private ArrayList<Card> cards;
	

	public Deck() {
		cards = new ArrayList<Card>();
	}

	public void add(Card c) {
		cards.add(c);
	}

	public void add(ArrayList<Card> cs) {
		for (Card c : cs) {
			cards.add(c);
		}
	}

	public void clear() {
		cards.clear();
	}

	public void shuffle(long rgenseed) {
		Random r = new Random(rgenseed);
		for (int i = 0; i < cards.size(); i++) {
			Card currCard = cards.get(i);
			int newI = r.nextInt(cards.size());
			Card newCard = cards.get(newI);
			cards.set(newI, currCard);
			cards.set(i, newCard);
		}
	}

	public int getSize() {
		return cards.size();
	}

	public Card getCard(int index) {
		return cards.get(index);
	}

	public Card getFromTop(int i) {
		if (i >= cards.size()) {
			return null;
		}
		return cards.get(cards.size() - 1 - i);
	}
	
	public Card getFirstCard() {
		return getFromTop(0);
	}
	
	public Card getSecondCard() {
		return getFromTop(1);
	}

	public Card getThirdCard() {
		return getFromTop(2);
	}
	
	public Card getFourthCard() {
		return getFromTop(3);
	}
	
	public Card pop() {
		return cards.remove(cards.size() - 1);
	}

	private ArrayList<Card> getCards() {
		return cards;
	}
	
	public boolean isEmpty() {
		return getSize() == 0;
	}
	
	public Deck popDeck(int count) {
		Deck d = new Deck();
		for (int i = 0; i < count; i++) {
			Card c = pop();
			d.add(c);
		}
		return d;
	}

	public ArrayList<Deck> split(int countdecks) {
		ArrayList<Deck> decks = new ArrayList<Deck>();
		for (int i = 0; i < countdecks; i++) {
			Deck deck = new Deck();
			decks. add(deck);
		}
		
		int indexCard = cards.size() - 1;
		int indexDeck = 0;
		
		while (indexCard >= 0) {
			Card c =cards.get(indexCard);
			decks.get(indexDeck).add(c);
			
			indexCard--;
			indexDeck++;
			indexDeck = indexDeck % countdecks;
		}
		return decks;
	}

	public void addDeckToBottom(Deck otherDeck) {
		ArrayList<Card> newCards = new ArrayList<Card>(cards.size() + otherDeck.getSize());
		newCards.addAll(otherDeck.getCards());
		newCards.addAll(cards);
		cards = newCards;
	}
	
	public void addDeckToTop(Deck otherDeck) {
		ArrayList<Card> newCards = otherDeck.getCards();
		add(newCards);
	}

}
